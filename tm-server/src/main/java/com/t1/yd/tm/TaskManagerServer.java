package com.t1.yd.tm;

import com.t1.yd.tm.component.Bootstrap;

public final class TaskManagerServer {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

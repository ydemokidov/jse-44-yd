package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.dto.response.user.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public @NotNull UserPasswordChangeResponse passwordChange(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserPasswordChangeRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @Nullable final String newPassword = request.getNewPassword();
        if (newPassword == null) throw new PasswordEmptyException();
        @NotNull final UserDTO userDTO = getServiceLocator().getUserService().setPassword(userId, newPassword);
        return new UserPasswordChangeResponse(userDTO);
    }

    @Override
    @WebMethod
    public @NotNull UserLockResponse lock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLockRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request, Role.ADMIN);
        @Nullable final String userId = sessionDTO.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final UserDTO userDTO = getServiceLocator().getUserService().findOneById(userId);
        if (userDTO == null) throw new UserNotFoundException();
        getServiceLocator().getUserService().lockByLogin(userDTO.getLogin());
        return new UserLockResponse(userDTO);
    }

    @Override
    @WebMethod
    public @NotNull UserUnlockResponse unlock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUnlockRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request, Role.ADMIN);
        @Nullable final String userId = sessionDTO.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final UserDTO userDTO = getServiceLocator().getUserService().findOneById(userId);
        if (userDTO == null) throw new UserNotFoundException();
        getServiceLocator().getUserService().unlockByLogin(userDTO.getLogin());
        return new UserUnlockResponse(userDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registry(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRegistryRequest request) {
        @NotNull final String login = request.getLogin();
        @NotNull final String password = request.getPassword();
        @NotNull final String email = request.getEmail();
        @NotNull final UserDTO userDTO = getServiceLocator().getAuthService().registry(login, password, email);

        return new UserRegistryResponse(userDTO);
    }

    @Override
    @WebMethod
    public @NotNull UserRemoveResponse remove(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserRemoveRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request, Role.ADMIN);
        @Nullable final String userId = sessionDTO.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final UserDTO userDTO = getServiceLocator().getUserService().removeById(userId);
        return new UserRemoveResponse(userDTO);
    }

    @Override
    @WebMethod
    public @NotNull UserUpdateProfileResponse updateProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserUpdateProfileRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        if (userId == null) throw new IdEmptyException();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMidName();
        @Nullable final UserDTO userDTO = getServiceLocator().getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(userDTO);
    }

}

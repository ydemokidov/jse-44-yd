package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IDtoUserOwnedRepository<E extends AbstractUserOwnedEntityDTO> extends IDtoRepository<E> {

    E add(@NotNull String userId, @NotNull E entity);

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    E removeById(@NotNull String userId, @NotNull String id);

    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
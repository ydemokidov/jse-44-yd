package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskCompleteByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
